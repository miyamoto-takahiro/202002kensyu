<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  // commom
  //inculude("./include/functions.php");

  include("./include/statics.php");

  // $pdo = initDB();

?>

<style>
  .table1 {   border: 1px solid gray;
  }
  .table1 th, .table1 td {
  border: 1px solid gray;
  }
  .table1 th {
  background-color: #c0c0c0;
  }
  .table1 {   width: 700px;
  }

  .rink{
  text-align: right ;
  }

</style>

<script>
  function conf(){
      if(window.confirm('新規社員登録してよろしいですか？')){
        document.e_worker.submit();
      }else{
        return false;
      }
  }
</script>

<html>
  <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>新規社員登録ページ</title>
  </head>
  <body>

    <h1><b>社員名簿システム</b></h1>

    <div class="rink">
      |<a href="index.php">トップ画面</a>
      |<a href="entry01.php">新規社員登録へ</a>|
    </div>

    <hr>

      <form method="post" action="entry02.php" name='e_worker'>

        <pre>
          <?php
          //var_dump($result);
          ?>
        </pre>

        <table class="table1" align="center">

          <tr>
            <th>名前</th>
            <td>
              <input type="text" name="name">※入力必須
            </td>
          </tr>

          <tr>
            <th>出身地</th>
            <td>
              <select name="pref">
                <option value="" selected>都道府県</option>
                 <?php
                  foreach ($pref_array as $key => $value) {
                    echo "<option value='" . $key . "'>" . $value . "</option>";
                  }
                 ?>
              </select>※入力必須
            </td>
          </tr>

          <tr>
            <th>性別</th>
            <td>
              <input type="radio" name="sex" value="1" checked>男
              <input type="radio" name="sex" value="2">女
            </td>
          </tr>

          <tr>
            <th>年齢</th>
            <td>
              <input type="number" name="age">※入力必須、半角で入力
            </td>
          </tr>

          <tr>
            <th>所属部署</th>
            <td>
              <input type="radio" name="section" value="1" checked>第一事業部
              <input type="radio" name="section" value="2">第二事業部
              <input type="radio" name="section" value="3">営業
              <input type="radio" name="section" value="4">総務
              <input type="radio" name="section" value="5">人事
            </td>
          </tr>

          <tr>
            <th>役職</th>
            <td>
              <input type="radio" name="grade" value="1" checked>事業部長
              <input type="radio" name="grade" value="2">部長
              <input type="radio" name="grade" value="3">チームリーダー
              <input type="radio" name="grade" value="4">リーダー
              <input type="radio" name="grade" value="5">メンバー
            </td>
          </tr>

        </table>

        <div class="rink">
          <input type="button" value="登録" onClick="conf();">
          <input type="reset" value="リセット">
        </div>

      </form>

    </body>
  </html>
