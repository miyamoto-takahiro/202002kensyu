-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時:
-- サーバのバージョン： 10.4.11-MariaDB
-- PHP のバージョン: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `miyamoto_kensyu`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `grade_master`
--

CREATE TABLE `grade_master` (
  `grade_ID` int(11) NOT NULL,
  `grade_name` varchar(40) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `grade_master`
--

INSERT INTO `grade_master` (`grade_ID`, `grade_name`) VALUES
(1, '事業部長'),
(2, '部長'),
(3, 'チームリーダー'),
(4, 'リーダー'),
(5, 'メンバー');

-- --------------------------------------------------------

--
-- テーブルの構造 `member`
--

CREATE TABLE `member` (
  `ID` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `ward` int(2) NOT NULL,
  `sex` int(2) NOT NULL,
  `age` int(3) NOT NULL,
  `grade` int(2) NOT NULL,
  `section` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `member`
--

INSERT INTO `member` (`ID`, `name`, `ward`, `sex`, `age`, `grade`, `section`) VALUES
(1, '相田たろう', 6, 1, 30, 1, 2),
(2, '福田さんぺい', 40, 1, 70, 2, 1),
(3, '上野とおる', 24, 1, 44, 4, 4),
(4, '遠藤キョウコ', 28, 2, 22, 3, 3),
(5, '太田かずき', 12, 1, 19, 3, 1),
(6, '片桐 はいり', 34, 2, 18, 4, 2),
(7, '菊本のぞむ', 40, 1, 33, 2, 1),
(8, '栗山はなこ', 30, 2, 100, 3, 3),
(9, 'ケイティ佐藤', 1, 2, 28, 2, 2),
(10, '寿ふとし', 3, 1, 40, 3, 4),
(11, '佐藤かける', 20, 1, 30, 2, 2),
(16, '中村あずき', 21, 2, 23, 2, 4),
(21, '佐宇かりな', 2, 2, 11, 3, 2),
(22, '小谷たかとし', 1, 1, 30, 2, 2),
(23, '福田さんぺい', 2, 2, 66, 5, 3),
(24, '二ノ宮まちこ', 6, 2, 66, 2, 2),
(25, '船井でんじろう', 3, 1, 28, 4, 2),
(26, '辺見うめ', 5, 2, 42, 5, 4),
(28, 'みちこ', 1, 2, 5, 2, 2),
(29, 'てすとですよ', 1, 1, 2147483647, 1, 1),
(31, 'はなこ', 1, 2, 10, 2, 2);

-- --------------------------------------------------------

--
-- テーブルの構造 `section_master`
--

CREATE TABLE `section_master` (
  `section_ID` int(11) NOT NULL,
  `section_name` varchar(40) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `section_master`
--

INSERT INTO `section_master` (`section_ID`, `section_name`) VALUES
(1, '第一事業部'),
(2, '第二事業部'),
(3, '営業'),
(4, '総務'),
(5, '人事');

-- --------------------------------------------------------

--
-- テーブルの構造 `テストテーブル`
--

CREATE TABLE `テストテーブル` (
  `id` int(10) NOT NULL,
  `dish_name` varchar(30) COLLATE utf8_bin NOT NULL,
  `jyanru` varchar(30) COLLATE utf8_bin NOT NULL,
  `nedan` int(10) NOT NULL,
  `memo` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- テーブルのデータのダンプ `テストテーブル`
--

INSERT INTO `テストテーブル` (`id`, `dish_name`, `jyanru`, `nedan`, `memo`) VALUES
(1, 'とり皮と長ネギ', '炒めもの', 750, '看板メニュー'),
(3, 'つくね豆腐ハンバーグ', 'おつまみ', 650, 'ビールに合う'),
(4, 'ラーメン', '麺類', 500, '醤油味'),
(5, '餃子', '中華', 300, '6個入り'),
(6, '北京ダッグ', 'アジアン', 2000, 'うまい');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `grade_master`
--
ALTER TABLE `grade_master`
  ADD PRIMARY KEY (`grade_ID`);

--
-- テーブルのインデックス `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`ID`);

--
-- テーブルのインデックス `section_master`
--
ALTER TABLE `section_master`
  ADD PRIMARY KEY (`section_ID`);

--
-- テーブルのインデックス `テストテーブル`
--
ALTER TABLE `テストテーブル`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `grade_master`
--
ALTER TABLE `grade_master`
  MODIFY `grade_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- テーブルのAUTO_INCREMENT `member`
--
ALTER TABLE `member`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- テーブルのAUTO_INCREMENT `section_master`
--
ALTER TABLE `section_master`
  MODIFY `section_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- テーブルのAUTO_INCREMENT `テストテーブル`
--
ALTER TABLE `テストテーブル`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
