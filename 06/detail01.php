<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  // commom
  //inculude("./include/functions.php");

  include("./include/statics.php");

  $DB_DSN = "mysql:host=localhost; dbname=miyamoto_kensyu; charset=utf8";
  $DB_USER = "php_user";
  $DB_PW = "ZbeA6Zi0VCsCcmcd";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  // $pdo = initDB();

  $query_str = "SELECT
                  m.ID,
                  m.name,
                  m.ward,
                  m.sex,
                  m.age,
                  gm.grade_name,
                  sm.section_name
                	FROM member AS m
                	LEFT JOIN section_master AS sm ON sm.section_ID = m.section
                	LEFT JOIN grade_master AS gm ON gm.grade_ID = m.grade

                  WHERE m.ID = " . $_GET['ID'] ;

    //echo $query_str;
    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchAll();

?>

<style>
  .table1 {   border: 1px solid gray;
  }
  .table1 th, .table1 td {
  border: 1px solid gray;
  }
  .table1 th {
  background-color: #c0c0c0;
  }
  .table1 {   width: 700px;
  }

  .rink{
  text-align: right ;
  }

</style>

<script>
  function conf(){
      if(window.confirm('削除してよろしいですか？')){
        document.del_worker.submit();
      }else{
        return false;
      }
  }
</script>

<html>
  <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>詳細ページ</title>
  </head>

  <body>

    <h1><b>社員名簿システム</b></h1>

    <div class="rink">
      |<a href="index.php">トップ画面</a>|
      <a href="entry01.php">新規社員登録へ</a>|
    </div>

    <hr>

        <pre>
          <?php
          //var_dump($result);
          ?>
        </pre>

        <table class="table1" align="center">

          <tr>
            <th>社員ID</th><td><?php echo $result[0]['ID']; ?></td>
          </tr>

          <tr>
            <th>名前</th><td><?php echo $result[0]['name']; ?></td>
          </tr>

          <tr>
            <th>出身地</th><td><?php echo $pref_array[$result[0]['ward']]; ?></td>
          </tr>

          <tr>
            <th>性別</th><td><?php echo $gender_array[$result[0]['sex']]; ?></td>
          </tr>

          <tr>
            <th>年齢</th><td><?php echo $result[0]['age']; ?></td>
          </tr>

          <tr>
            <th>所属部署</th><td><?php echo $result[0]['section_name']; ?></td>
          </tr>

          <tr>
            <th>役職</th><td><?php echo $result[0]['grade_name']; ?></td>
          </tr>

        </table>

        <div class="rink">
          <form method="post" action="entry_update01.php"  >

            <input type="submit" value="編集">
            <input type="hidden" value="<?php echo $result[0]['ID']; ?>" name="id">

          </form>

          <form method="post" action="delete01.php" name='del_worker'>

            <input type="button" value="削除" onClick="conf();">
            <input type="hidden" value="<?php echo $result[0]['ID']; ?>" name="id">

          </form>
        </div>
  </body>
</html>
