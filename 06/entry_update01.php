<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
<?php
  // commom
  //inculude("./include/functions.php");

  include("./include/statics.php");

  // $pdo = initDB();

  $DB_DSN = "mysql:host=localhost; dbname=miyamoto_kensyu; charset=utf8";
  $DB_USER = "php_user";
  $DB_PW = "ZbeA6Zi0VCsCcmcd";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $query_str = "SELECT
                  m.ID,
                  m.name,
                  m.ward,
                  m.sex,
                  m.age,
                  m.grade,
                  m.section
                  FROM member AS m

                  WHERE m.ID = " . $_POST['id'] ;

  //echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();

?>

<pre>
  <?php
    //var_dump($_GET);
  ?>
</pre>

<style>
  .table1 {   border: 1px solid gray;
  }
  .table1 th, .table1 td {
  border: 1px solid gray;
  }
  .table1 th {
  background-color: #c0c0c0;
  }
  .table1 {   width: 700px;
  }

  .rink{
  text-align: right ;
  }

</style>

<script>
  function conf(){
      if(window.confirm('変更してよろしいですか？')){
        document.eu_worker.submit();
      }else{
        return false;
      }
  }
</script>

<html>
  <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>社員情報編集ページ</title>
  </head>

  <body>

    <h1><b>社員名簿システム</b></h1>

    <div class="rink">
      |<a href="index.php">トップ画面</a>
      |<a href="entry01.php">新規社員登録へ</a>|
    </div>

    <hr>

      <form method="post" action="entry_update02.php" name='eu_worker'>

        <pre>
          <?php
          //var_dump($result);
          ?>
        </pre>

        <table class="table1" align="center">

          <tr>
            <th>ID</th>
            <td>
              <?php echo $result[0]['ID']; ?>
            </td>
          </tr>

          <tr>
            <th>名前</th>
            <td>
              <input type="text" name="name" value="<?php echo $result[0]['name']; ?>">※入力必須
            </td>
          </tr>

          <tr>
            <th>出身地</th>
            <td>
              <select name="pref">
                <?php
                  foreach ($pref_array as $key => $value){
                    echo "<option value='" . $key . "'";
                    if($result[0]['ward'] == $key){ echo " selected ";}
                    echo ">" . $value . "</option>";
                  }
                ?>
              </select>※入力必須
            </td>
          </tr>

          <tr>
            <th>性別</th>
            <td>

              <input type="radio" name="sex" value="1"
                <?php if(isset($result[0]['sex']) && $result[0]['sex'] == "1"){echo "checked"; } ?>
              >男

              <input type="radio" name="sex" value="2"
                <?php if(isset($result[0]['sex']) && $result[0]['sex'] == "2"){echo "checked"; } ?>
              >女

            </td>
          </tr>

          <tr>
            <th>年齢</th>
            <td>
              <input type="number" name="age" value="<?php echo $result[0]['age']; ?>" >入力必須、半角で入力
            </td>
          </tr>

          <tr>
            <th>所属部署</th>
            <td>

              <input type="radio" name="section" value="1"
                <?php if(isset($result[0]['section']) && $result[0]['section'] == "1"){echo "checked"; } ?>
              >第一事業部

              <input type="radio" name="section" value="2"
                <?php if(isset($result[0]['section']) && $result[0]['section'] == "2"){echo "checked"; } ?>
              >第二事業部

              <input type="radio" name="section" value="3"
                <?php if(isset($result[0]['section']) && $result[0]['section'] == "3"){echo "checked"; } ?>
              >営業

              <input type="radio" name="section" value="4"
                <?php if(isset($result[0]['section']) && $result[0]['section'] == "4"){echo "checked"; } ?>
              >総務

              <input type="radio" name="section" value="5"
                <?php if(isset($result[0]['section']) && $result[0]['section'] == "5"){echo "checked"; } ?>
              >人事

            </td>
          </tr>

          <tr>
            <th>役職</th>
            <td>
              <input type="radio" name="grade" value="1"
                <?php if(isset($result[0]['grade']) && $result[0]['grade'] == "1"){echo "checked"; } ?>
              >事業部長

              <input type="radio" name="grade" value="2"
                <?php if(isset($result[0]['grade']) && $result[0]['grade'] == "2"){echo "checked"; } ?>
              >部長

              <input type="radio" name="grade" value="3"
                <?php if(isset($result[0]['grade']) && $result[0]['grade'] == "3"){echo "checked"; } ?>
              >チームリーダー

              <input type="radio" name="grade" value="4"
                <?php if(isset($result[0]['grade']) && $result[0]['grade'] == "4"){echo "checked"; } ?>
              >リーダー

              <input type="radio" name="grade" value="5"
                <?php if(isset($result[0]['grade']) && $result[0]['grade'] == "5"){echo "checked"; } ?>
              >メンバー

            </td>
          </tr>

        </table>

        <div class="rink">
          <input type="button" value="登録" onClick="conf();">
          <!-- <input type="submit" value="登録_sub">requiredを使用する場合 -->
          <input type="hidden" value="<?php echo $result[0]['ID']; ?>" name="ID">

          <input type="reset"　value="リセット">
        </div>

      </form>
  </body>
</html>
