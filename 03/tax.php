<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>消費税計算ページ</title>
    <link rel="stylesheet" href="style.css">

    <style>
      .table1 {
      border: 1px solid gray;
      }
      .table1 th, .table1 td {
      border: 1px solid gray;
      }
    </style>

  </head>

  <body>

    <form method="POST" action="tax.php">
      <table class="table1">
        <tr>
          <th>商品名</th><th>価格（単位：円、税抜き）</th><th>個数</th><th>税率</th>
        </tr>

        <tr>
          <td><input type="text" name="syouhin01"></td>
          <td><input type="number" name="kakaku01"></td>
          <td><input type="number" name="kosuu01">個</td>
          <td><input type="radio" name="zeiritu01" value="1.08" checked>8%
          <input type="radio" name="zeiritu01" value="1.1">10%</td>

        </tr>

        <tr>
          <td><input type="text" name="syouhin02"></td>
          <td><input type="number" name="kakaku02"></td>
          <td><input type="number" name="kosuu02">個</td>
          <td><input type="radio" name="zeiritu02" value="1.08" checked>8%
          <input type="radio" name="zeiritu02" value="1.1">10%</td>
        </tr>

        <tr>
          <td><input type="text" name="syouhin03"></td>
          <td><input type="number" name="kakaku03"></td>
          <td><input type="number" name="kosuu03">個</td>
          <td><input type="radio" name="zeiritu03" value="1.08" checked>8%
          <input type="radio" name="zeiritu03" value="1.1">10%</td>
        </tr>

        <tr>
          <td><input type="text" name="syouhin04"></td>
          <td><input type="number" name="kakaku04"></td>
          <td><input type="number" name="kosuu04">個</td>
          <td><input type="radio" name="zeiritu04" value="1.08" checked>8%
          <input type="radio" name="zeiritu04" value="1.1">10%</td>
        </tr>

        <tr>
          <td><input type="text" name="syouhin05"></td>
          <td><input type="number" name="kakaku05"></td>
          <td><input type="number" name="kosuu05">個</td>
          <td><input type="radio" name="zeiritu05" value="1.08" checked>8%
          <input type="radio" name="zeiritu05" value="1.1">10%</td>
        </tr>

      </table>

      <br/>

      <input type="submit" value="送信">
      <input type="reset" value="リセット">

      <br/>

      <table class="table1">
          <tr>
            <th>商品名</th><th>価格（単位：円、税抜き）</th><th>個数</th><th>税率</th><th>小計(単位：円)</th>
          </tr>

          <tr>
            <td><?php echo $_POST['syouhin01'];?></td>
            <td><?php echo $_POST['kakaku01'];?></td>
            <td><?php echo $_POST['kosuu01'];?>個</td>
            <td><?php echo $_POST['zeiritu01'];?></td>
            <td><?php echo $_POST['kakaku01'] * $_POST['kosuu01'] * $_POST['zeiritu01']; ?></td>
            <?php $price = $_POST['kakaku01'] * $_POST['kosuu01'] * $_POST['zeiritu01']; ?>
            <?php $total = 0; ?>
            <?php $total = $total + $price; ?>
          </tr>

          <tr>
            <td><?php echo $_POST['syouhin02'];?></td>
            <td><?php echo $_POST['kakaku02'];?></td>
            <td><?php echo $_POST['kosuu02'];?>個</td>
            <td><?php echo $_POST['zeiritu02'];?></td>
            <td><?php echo $_POST['kakaku02'] * $_POST['kosuu02'] * $_POST['zeiritu02']; ?></td>
            <?php $price = $_POST['kakaku02'] * $_POST['kosuu02'] * $_POST['zeiritu02']; ?>
            <?php $total = $total + $price; ?>
          </tr>

          <tr>
            <td><?php echo $_POST['syouhin03'];?></td>
            <td><?php echo $_POST['kakaku03'];?></td>
            <td><?php echo $_POST['kosuu03'];?>個</td>
            <td><?php echo $_POST['zeiritu03'];?></td>
            <td><?php echo $_POST['kakaku03'] * $_POST['kosuu03'] * $_POST['zeiritu03']; ?></td>
            <?php $price = $_POST['kakaku03'] * $_POST['kosuu03'] * $_POST['zeiritu03']; ?>
            <?php $total = $total + $price; ?>
          </tr>

          <tr>
            <td><?php echo $_POST['syouhin04'];?></td>
            <td><?php echo $_POST['kakaku04'];?></td>
            <td><?php echo $_POST['kosuu04'];?>個</td>
            <td><?php echo $_POST['zeiritu04'];?></td>
            <td><?php echo $_POST['kakaku04'] * $_POST['kosuu04'] * $_POST['zeiritu04']; ?></td>
            <?php $price = $_POST['kakaku04'] * $_POST['kosuu04'] * $_POST['zeiritu04']; ?>
            <?php $total = $total + $price; ?>
          </tr>

          <tr>
            <td><?php echo $_POST['syouhin05'];?></td>
            <td><?php echo $_POST['kakaku05'];?></td>
            <td><?php echo $_POST['kosuu05'];?>個</td>
            <td><?php echo $_POST['zeiritu05'];?></td>
            <td><?php echo $_POST['kakaku05'] * $_POST['kosuu05'] * $_POST['zeiritu05']; ?></td>
            <?php $price = $_POST['kakaku05'] * $_POST['kosuu05'] * $_POST['zeiritu05']; ?>
            <?php $total = $total + $price; ?>
          </tr>

          <tr>
            <td>合計</td>
            <td><?php echo $total . "円" ; ?> </td>
          </tr>

      </table>
    </form>
  </body>
<html>
