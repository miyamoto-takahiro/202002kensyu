<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>loop</title>
    <link rel="stylesheet" href="style.css">

    <style>
      .table1 {
      border: 1px solid gray;
      }
      .table1 th, .table1 td {
      border: 1px solid gray;
      }
    </style>

  </head>

  <body>
    <table class="table1">
      <?php
      for($i = 0 ; $i < 10 ; $i++){
      echo "<td>清話会ビル</td><td>msビル</td><td>水道橋</td>";
      }
      ?>
    </table>
  </body>
</html>
